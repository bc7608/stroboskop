# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```git clone 
```

Naloga 6.2.3:
https://bitbucket.org/bc7608/stroboskop/commits/e36557e83ad38cb1d993d888900e38ec18908008
https://bitbucket.org/bc7608/stroboskop/commits/e7ed5bedc74adeb87ea6ef8980140f964b3dcdfe

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/bc7608/stroboskop/commits/9fbf36fd18e4dac43a6bf9900da46edd48f7c3f4

Naloga 6.3.2:
https://bitbucket.org/bc7608/stroboskop/commits/cd24470479dbf3051ae66e7a19547e74b32c170c

Naloga 6.3.3:
https://bitbucket.org/bc7608/stroboskop/commits/4a337fcd2f7f5a17fe3de00dcb41462c783e6cfd

Naloga 6.3.4:
https://bitbucket.org/bc7608/stroboskop/commits/2d09bf9177b3b35250055c70d2b9f1aefb63dced

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/bc7608/stroboskop/commits/a54935839891d6f5bba400631d7a972a8d1673b3

Naloga 6.4.2:
https://bitbucket.org/bc7608/stroboskop/commits/e41b2f9a78828f3cdff3f70ae73dee34876fe053

Naloga 6.4.3:
https://bitbucket.org/bc7608/stroboskop/commits/9c213656951f00f72b2f6ccc1c871b885fd21a62

Naloga 6.4.4:
https://bitbucket.org/bc7608/stroboskop/commits/24ed02415f8bd1e73c01144dba939b539c3b012d